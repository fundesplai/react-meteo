import React, { useState, useEffect } from "react";

import { Container, Table } from "reactstrap";
import styled from "styled-components";

const IconoCentrado = styled.i`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;
  width: 100%;
`;

const Meteo = () => {
  const [llista, setLlista] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const ciutat = "granada,es";
    const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
    const funcio = "forecast";
    const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setLlista(data.list);
        setLoading(false);
      })
      .catch((error) => setError(true));


  }, []);



  
  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }


  if (loading) {
    return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
  }

  const filas = llista.filter((el, idx) => idx%4===0).map(el => {
      const data = new Date(el.dt*1000);
      const icon = el.weather[0].icon;
      return (
          <tr>
              <td>{data.toLocaleString()}</td>
              <td>{el.main.temp}</td>
              <td><img alt={icon} src={`http://openweathermap.org/img/wn/${icon}@2x.png`} /></td>
          </tr>
      );
  })

  return (
    <Container>
      <br />
      <br />
      <Table>
          <thead>
              <tr>
                  <th>Fecha/hora</th>
                  <th>Temp</th>
                  <th>Icon</th>
              </tr>
          </thead>
          <tbody>
              {filas}
          </tbody>
      </Table>

    </Container>
  );
};

export default Meteo;
